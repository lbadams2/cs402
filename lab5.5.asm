.data 0x10000000
char1: .byte 'a' # reserve space for a byte
double1: .double 1.1 # reserve space for a double
char2: .byte 'b' # b is 0x62 in ASCII
half1: .half 0x8001 # reserve space for a half-word (2 bytes)
char3: .byte 'c' # c is 0x63 in ASCII
word1: .word 0x56789abc # reserve space for a word
char4: .byte 'd' # d is 0x64 in ASCII
word2: .word 0
	
	.text 
	.globl main
	
main: 
li $a1, 0
lb $t0, word1($a1) 
addi $a1, $a1, 1
lb $t1, word1($a1) 
addi $a1, $a1, 1
lb $t2, word1($a1) 
addi $a1, $a1, 1
lb $t3, word1($a1) 
li $a1, 0
lbu $t4, word1($a1) 
addi $a1, $a1, 1
lbu $t5, word1($a1) 
addi $a1, $a1, 1
lbu $t6, word1($a1) 
addi $a1, $a1, 1
lbu $t7, word1($a1) 
lh $t8, word1
lhu $t9, word1
li $a1, 0
sb $t3, word2($a1)
addi $a1, $a1, 1
sb $t2, word2($a1)
addi $a1, $a1, 1
sb $t1, word2($a1)
addi $a1, $a1, 1
sb $t0, word2($a1)
jr $ra # return from main
