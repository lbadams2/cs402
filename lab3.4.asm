.data 0x10000000 
my_array: .space 10
limit: .word 0x000a
initial_value: .word 0x0002
		
		.text
		.globl main
main:	addu $s0, $ra, $0# save $31 in $16		

		
		lw $a1, limit
		
		andi $t0, $a0, 0	#i is in $t0 initialized to 0 
		sw $t4, initial_value # j = initial_value
Loop:	ble $a1, $t0, Exit	# exit if limit <= i
		la $a0, my_array # load address of current element pointed to in array
		lw $t1, 0($a0) # load value of array stored in currently pointed to address
		sw $t1, 0($t4) # store j in current array cell
		addiu $a0, $a0, 4	# point to next element of array
		addi $t0, $t0, 1	# i = i + 1
		addi $t4, $t4, 1	# j = j + 1
		j Loop

Exit: 	
				
				
# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 # return address back in $31
		jr $ra # return from main		