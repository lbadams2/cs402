.data 0x10000000
word1: .word 0x89abcdef # reserve space for a word

	.text 
	.globl main
main: 
	la $t4, word1
	lwl $t0, 0($t4)
	lwl $t1, 1($t4)
	lwl $t2, 2($t4)
	lwl $t3, 3($t4)
	
	jr $ra # return from main