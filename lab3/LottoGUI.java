package lab3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LottoGUI extends JFrame {

	private static final long serialVersionUID = 2230782436491815211L;
	private List<Integer> winners;
	private List<Integer> selected;
	private int balance;
	private Map<String, JButton> leftButtons;
	private Map<String, JButton> numberButtons;
	private Map<String, JLabel> labels;

	public static void main(String[] args) {
		new LottoGUI();
	}

	public LottoGUI() {
		super("CompSci Lotto");
		balance = 10;
		JPanel buttonPane = new JPanel(new GridLayout(7, 7));
		buttonPane.setName("buttonPane");
		numberButtons = new HashMap<String, JButton>();

		for (int i = 0; i < 49; i++) {
			JButton button = initializeButton(String.valueOf(i), null,
					Color.BLACK, true, new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							numberChosen((JButton) e.getSource());
						}
					});
			buttonPane.add(button);
			numberButtons.put(button.getName(), button);
		}

		JPanel pane2 = new JPanel(new GridBagLayout());
		leftButtons = new HashMap<String, JButton>();
		pane2.setName("pane2");

		JButton draw = initializeButton("Draw", Color.BLACK, Color.BLACK,
				false, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						draw(e.getModifiers());
					}
				});
		draw.setMargin(new Insets(0,0,0,0));
		leftButtons.put(draw.getName(), draw);

		JButton reset = initializeButton("Reset", Color.BLACK, Color.RED, true,
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						reset();
					}
				});
		reset.setMargin(new Insets(0,0,0,0));
		leftButtons.put(reset.getName(), reset);

		JButton quit = initializeButton("Quit", Color.BLACK, Color.WHITE, true,
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						quit();
					}
				});
		leftButtons.put(quit.getName(), quit);

		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 0;
		c2.weighty = .2;
		pane2.add(draw, c2);
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 1;
		c2.weighty = .2;
		pane2.add(reset, c2);
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 2;
		c2.weighty = .2;
		pane2.add(quit, c2);

		labels = new HashMap<String, JLabel>();
		JLabel left = new JLabel("Picking numbers...", JLabel.LEFT);
		left.setName("left");
		labels.put(left.getName(), left);
		JLabel right = new JLabel("10$", JLabel.RIGHT);
		right.setName("right");
		labels.put(right.getName(), right);

		JPanel p = new JPanel(new BorderLayout());
		p.setName("bottomPane");
		p.add(left, BorderLayout.WEST);
		p.add(right, BorderLayout.EAST);

		add(buttonPane, BorderLayout.EAST);
		add(pane2, BorderLayout.WEST);
		add(p, BorderLayout.SOUTH);
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		winners = new ArrayList<Integer>();
		selected = new ArrayList<Integer>();
	}

	private void numberChosen(JButton button) {
		Component drawButton = leftButtons.get("Draw");

		if (button.getBackground() == Color.BLUE) {
			button.setBackground(null);
			selected.remove((Object) Integer.parseInt(button.getName()));
//			System.out.println("After removal: " + selected);
			drawButton.setEnabled(false);
			drawButton.setForeground(Color.BLACK);
			return;
		}

		if (selected.size() >= 6)
			return;

		Integer chosenInt = new Integer(button.getText());
		selected.add(chosenInt);
		button.setBackground(Color.BLUE);
		drawButton.setEnabled(false);
		drawButton.setForeground(Color.BLACK);

		if (selected.size() == 6) {
			drawButton.setEnabled(true);
			drawButton.setForeground(Color.GREEN);
		}
	}

	private static JButton initializeButton(String text, Color back,
			Color textColor, boolean active, ActionListener listener) {
		JButton init = new JButton(text);
		init.setBackground(back);
		init.setForeground(textColor);
		init.setEnabled(active);
		init.addActionListener(listener);
		init.setPreferredSize(new Dimension(60, 40));
		init.setName(text);
		return init;
	}

	private void pickWinner(boolean fix) {
		balance -= 1;
		winners.clear();
		int randomNum;
		int matches = 0;
		if (fix == false) {
			for (int i = 0; i < 6; i++) {
				do {
					randomNum = (int) (Math.random() * ((48 - 0) + 1));
				} while (winners.contains(randomNum));
				if (selected.contains(randomNum)) {
					matches++;
					numberButtons.get(String.valueOf(randomNum)).setBackground(
							Color.GREEN);
				}
				winners.add(randomNum);
			}
			for (Integer i : selected) {
				JButton b = numberButtons.get(String.valueOf(i));
				if (b.getBackground() != Color.GREEN)
					b.setBackground(Color.RED);
			}
		}
		else {
			winners.addAll(selected);
			for(String name : numberButtons.keySet())
				if(selected.contains(Integer.parseInt(name)))
					numberButtons.get(name).setBackground(Color.GREEN);
			matches = 6;
		}
			
//		System.out.println("Winning numbers: " + winners);
		if (matches == 6)
			balance += 12;
		else
			balance += matches;
		labels.get("right").setText("$" + balance);
	}

	private void draw(int modifiers) {
		if ((modifiers & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK)
			pickWinner(true);
		else
			pickWinner(false);
	}

	private void reset() {
		winners.clear();
		selected.clear();
		for (String buttonName : numberButtons.keySet())
			numberButtons.get(buttonName).setBackground(null);
		JButton draw = leftButtons.get("Draw");
		draw.setForeground(Color.BLACK);
		draw.setEnabled(false);
	}

	private void quit() {
		dispose();
	}
}