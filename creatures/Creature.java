package creatures;

public abstract class Creature extends Thing {
	
	private Thing lastEaten;
	
	public Creature(String name) {
		super(name);
	}
	
	public void eat(Thing aThing) {
		System.out.println(toString() + " has just eaten " + aThing.toString());
		lastEaten = aThing;
	}
	
	public abstract void move();
	
	public void whatDidYouEat() {
		if(lastEaten == null)
			System.out.println(toString() + " has had nothing to eat!");
		else
			System.out.println(toString() + " has eaten a " + lastEaten);
	}
}
