package creatures;

public class TestCreature {

	private final static int CREATURE_COUNT = 6;
	private final static int THING_COUNT = 10;
	
	public TestCreature() {}
	
	public static void main(String[] args){
		Creature creatures[] = new Creature[CREATURE_COUNT];
		Thing things[] = new Creature[THING_COUNT];
		
		creatures[0] = new Ant("Bruce");
		creatures[1] = new Ant("Bob");
		creatures[2] = new Bat("Todd");
		creatures[3] = new Bat("George");
		creatures[4] = new Fly("Delores");
		creatures[5] = new Tiger("Sandy");
		
		for (int i = 0; i < THING_COUNT; i++) {
			if(i >= CREATURE_COUNT - 1) {
				things[i] = new Tiger("tiger" + i);
				continue;
			}
			things[i] = creatures[i];
		}
		
		System.out.println("Things:");
		for(Thing t : things)
			System.out.println(t);
		System.out.println("");
		
		System.out.println("Creatures:");
		for(Creature c : creatures) {
			System.out.println(c);
			c.move();
		}
		System.out.println("");
		
		creatures[0].eat(creatures[1]);
		creatures[0].whatDidYouEat();
		System.out.println("");
		creatures[2].eat(creatures[1]);
		creatures[2].eat(new Thing("thing"));
		creatures[2].whatDidYouEat();
		System.out.println("");
		creatures[4].eat(creatures[5]);
		creatures[4].eat(new Thing("thing"));
		creatures[4].whatDidYouEat();
		System.out.println("");
		creatures[5].eat(creatures[4]);
		creatures[5].whatDidYouEat();
	}
}
