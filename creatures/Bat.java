package creatures;

public class Bat extends Creature implements Flyer {

	public Bat(String name) {
		super(name);
	}

	@Override
	public void fly() {
		System.out.println(this + " is swooping through the dark");
	}

	@Override
	public void move() {
		fly();
	}
	
	public void eat(Thing aThing) {
		if(!(aThing instanceof Creature)) {
			System.out.println(this + " will not eat " + aThing);
			return;
		}
		else if(!(aThing instanceof Thing))
			return;
		super.eat(aThing);
	}
	
	public void whatDidYouEat() {
		super.whatDidYouEat();
	}
}
