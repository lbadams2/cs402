package creatures;

public class Fly extends Creature implements Flyer {

	public Fly(String name) {
		super(name);
	}

	@Override
	public void fly() {
		System.out.println(this + " is buzzing around in flight");
	}

	@Override
	public void move() {
		fly();
	}

	public void eat(Thing aThing) {
		if(aThing instanceof Creature)
			System.out.println(this + " will not eat " + aThing);
		else
			super.eat(aThing);
	}
	
	public void whatDidYouEat() {
		super.whatDidYouEat();
	}
}
