package creatures;

public class Ant extends Creature{

	public Ant(String name) {
		super(name);
	}

	public void move() {
		System.out.println(this + " is crawling around");
	}

	@Override
	public void eat(Thing aThing) {
		super.eat(aThing);
	}
	
	public void whatDidYouEat() {
		super.whatDidYouEat();
	}
}
