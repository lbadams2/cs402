.data 0x10000000
word1: .word 0x89abcdef # reserve space for a word

	.text 
	.globl main
main: 
	la $t3, word1
	lwr $t4, 0($t3)
	lwr $t5, 1($t3)
	lwr $t6, 2($t3)
	lwr $t7, 3($t3)
	
	jr $ra # return from main