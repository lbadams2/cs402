.data 0x10000000 
var1: .word 0x0002
limit: .word 0x0064

		.text
		.globl main
main:	addu $s0, $ra, $0# save $31 in $16
		lw $a0, var1
		lw $a1, limit
		
		move $t0, $a0	#i is in $t0  
Loop:	ble $a1, $t0, Exit	# exit if limit <= i
		addi $a0, $a0, 1	# var1 = var1 + 1
		addi $t0, $t0, 1	# i = i + 1
		j Loop

Exit: 	sw $a0, var1
		
		
# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 # return address back in $31
		jr $ra # return from main		