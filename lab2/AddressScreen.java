package lab2;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AddressScreen extends JFrame {

	private static final long serialVersionUID = 2230782436491815211L;

	public static void main(String[] args) {
		new AddressScreen();
	}
	
	public AddressScreen() {
		JFrame frame = new JFrame("Address Information");
		
		String[] labels = {"Name", "Address", "City", "State", "Zip"};
		int numPairs = labels.length;

		JPanel pane = new JPanel(new GridLayout(0,2));
		for (int i = 0; i < numPairs; i++) {
		    JLabel l = new JLabel(labels[i], JLabel.LEFT);
		    pane.add(l);
		    JTextField textField = new JTextField(10);
		    l.setLabelFor(textField);
		    pane.add(textField);
		}
		
		JPanel pane2 = new JPanel(new FlowLayout());
		pane2.add(new JButton("Add"));
		pane2.add(new JButton("Modify"));
		pane2.add(new JButton("Delete"));
		
		JPanel pane3 = new JPanel(new GridLayout(2, 1));
		pane3.add(pane);
		pane3.add(pane2);
		
		frame.add(pane3);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}