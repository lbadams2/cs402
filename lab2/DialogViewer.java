package lab2;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DialogViewer extends JFrame {

	private static final long serialVersionUID = -6038504422059416363L;

	public static void main(String[] args) {
		if ("f".equals(args[0])) 
			new DialogViewer().fileChooser();
		else if ("c".equals(args[0]))
			new DialogViewer().confirm();
		else
			new DialogViewer().message();
	}

	private void fileChooser() {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(DialogViewer.this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
			System.out.println("You chose to open this file: " +
		            fc.getSelectedFile().getName());
		else
			System.out.println("You didn't select a file");
		System.exit(NORMAL);
	}

	private void confirm() {
		Object[] options = {"Yes", "No", "Cancel"};
		int reply = JOptionPane.showOptionDialog(this, "Are you sure?", "Pickles", 
				JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
				null, options, options[0]);
		if(reply == JOptionPane.YES_OPTION)
			System.out.println("Yes");
		else if(reply == JOptionPane.NO_OPTION)
			System.out.println("No");
		else
			System.out.println("Cancel");
		System.exit(NORMAL);
	}

	private void message() {
		JOptionPane.showMessageDialog(this, "Liam Adams");
		System.exit(NORMAL);
	}
}