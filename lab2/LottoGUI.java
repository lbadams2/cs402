package lab2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LottoGUI extends JFrame {

	private static final long serialVersionUID = 2230782436491815211L;

	public static void main(String[] args) {
		new LottoGUI();
	}
	
	public LottoGUI() {
		JFrame frame = new JFrame("CompSci Lotto");
		JPanel buttonPane = new JPanel(new GridLayout(7,7));
		
		for(int i = 0; i < 49; i++) {
			JButton button = new JButton(String.valueOf(i));
			button.setPreferredSize(new Dimension(60, 40));
			buttonPane.add(button);
		}
		
		JPanel pane2 = new JPanel(new GridBagLayout());
		JButton draw = new JButton("Draw");
		draw.setForeground(Color.GREEN);
		draw.setBackground(Color.BLACK);
		JButton reset = new JButton("Reset");
		reset.setForeground(Color.RED);
		reset.setBackground(Color.BLACK);
		JButton quit = new JButton("Quit");
		quit.setForeground(Color.WHITE);
		quit.setBackground(Color.BLACK);
		
		GridBagConstraints c2 = new GridBagConstraints();
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 0;
		c2.weighty = .2;
		pane2.add(draw, c2);
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 1;
		c2.weighty = .2;
		pane2.add(reset, c2);
		c2.fill = GridBagConstraints.BOTH;
		c2.gridx = 0;
		c2.gridy = 2;
		c2.weighty = .2;
		pane2.add(quit, c2);
		
		JLabel left = new JLabel("Picking numbers...", JLabel.LEFT);
		JLabel right = new JLabel("10$", JLabel.RIGHT);
		JPanel p = new JPanel(new BorderLayout());
		p.add(left, BorderLayout.WEST);
		p.add(right, BorderLayout.EAST);

		frame.add(buttonPane, BorderLayout.EAST);
		frame.add(pane2, BorderLayout.WEST);
		frame.add(p, BorderLayout.SOUTH);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}