.data 0x10000000
ch1: .byte 'a' # reserve space for a byte
word1: .word 0x89abcdef # reserve space for a word
ch2: .byte 'b' # reserve space for a byte
word2: .word 0 # reserve space for a word

	.text
	.globl main
	
main: 
	lw $t0, word1
	lw $t1, word2
	sw $t0, word2
	sw $t1, word1
	jr $ra # return		  