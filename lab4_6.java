
public class lab4_6 {

	public static void main(String[] args){
		System.out.println("(0,0) is " + ackermann(0,0));
		System.out.println("(0,2) is " + ackermann(0,2));
		System.out.println("(1,0) is " + ackermann(1,0));
		System.out.println("(2,3) is " + ackermann(2,3));
		System.out.println("(2,4) is " + ackermann(2,4));
		System.out.println("(2,5) is " + ackermann(2,5));
		System.out.println("(2,6) is " + ackermann(2,6));
		System.out.println("(3,1) is " + ackermann(3,1));
		System.out.println("(3,2) is " + ackermann(3,2));
		System.out.println("(3,3) is " + ackermann(3,3));
	}
	
	private static int ackermann(int x, int y){
		if(x == 0)
			return y + 1;
		else if(y == 0)
			return ackermann(x-1, 1);
		else
			return ackermann(x-1, ackermann(x, y-1));
	}
}
