.data 0x10000000
Ask_Input:	.asciiz "Please enter an integer: "
Msg:		.asciiz "If bytes were layed in reverse order the number would be: "
user1: .word 0
tmp:   .word 0

		 .text
		 .globl main
	
main: 	  addi $sp, $sp, -4 # must save $ra since I’ll have a call
		  sw $ra, 4($sp) 
		  
		  la $a0, Ask_Input # load address of Ask_Input into $a0
		  li $v0, 4 # opcode for print string
		  syscall
			
		  li $v0, 5 # opcode for read integer
		  syscall # wait for input
		  sw $v0, user1
		  la $a0, user1
		  jal Reverse_bytes
		  
		  move $t0, $v0
		  la $a0, Msg # 
		  li $v0, 4 # opcode for print string
		  syscall
			
		  li $v0, 1 # opcode for print integer
		  add $a0, $t0, $0
		  syscall # wait for input
		  
		  lw $ra, 4($sp) # restore the return address in $ra
		  addi $sp, $sp, 4
		  jr $ra # return from main
		  
Reverse_bytes:
			subu $sp, $sp, 4
			sw $ra, 4($sp) # save the return address on stack
			li $a1, 0
			lb $t0, user1($a1) 
			addi $a1, $a1, 1
			lb $t1, user1($a1) 
			addi $a1, $a1, 1
			lb $t2, user1($a1) 
			addi $a1, $a1, 1
			lb $t3, user1($a1) 

			li $a1, 0
			sb $t3, tmp($a1)
			addi $a1, $a1, 1
			sb $t2, tmp($a1)
			addi $a1, $a1, 1
			sb $t1, tmp($a1)
			addi $a1, $a1, 1
			sb $t0, tmp($a1)
			lw $ra, 4($sp) # get the return address
			addu $sp, $sp, 12 # adjust the stack pointer, don't care about x
			jr $ra # return