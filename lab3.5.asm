.data 0x10000000 
Ask_Input:	.asciiz "Please enter an integer: "
newline:	.asciiz "\n"
msg:		.asciiz "I'm far away"
msg2:		.asciiz "I'm nearby"

		.text
		.globl main
main:	addu $s0, $ra, $0# save $31 in $16		
		
		la $a0, Ask_Input # load address of Ask_Input into $a0
		li $v0, 4 # opcode for print string
		syscall
		
		li $v0, 5 # opcode for read integer
		syscall # wait for input
		move $t0, $v0 # move input to $t0
		
		la $a0, newline # load address of newline into $a0
		li $v0, 4 # opcode for print string
		syscall
		
		la $a0, Ask_Input # load address of Ask_Input into $a0
		li $v0, 4 # opcode for print string
		syscall
		
		li $v0, 5 # opcode for read integer
		syscall # wait for input
		move $t1, $v0 # move input to $t1
		
		beq $t0, $t1, Far
		
		la $a0, msg2 # load address of msg into $a0
		li $v0, 4 # opcode for print string
		syscall
		
		j Exit
		
Far:    la $a0, msg # load address of msg into $a0
		li $v0, 4 # opcode for print string
		syscall
		
Exit:	# restore now the return address in $ra and return from main
		addu $ra, $0, $s0 # return address back in $31
		jr $ra # return from main		